## 소개
<img src="images/Cute_Developer_8bit.jpg" width="150"></img>
- 이름: 최재오
- 군필여부: 28사단 C4I 지휘체계운용병 만기전역 
- 취미: 게임, 영화감상, 운동
- Email: chlwodh97@gmail.com
- <a href="https://flowery-clementine-57f.notion.site/Portfolio-7915887a89ed4b68b82f444f58285d4c">노션 바로가기</a>

```
길을 잃는 다는 것은 곧 길을 알게된다는 것이다.
```


## 회사경력
- (2021 ~ 현재)
- 쓰리텍(2020 ~ 2021) 키오스크 조립 및 설치 , 프로그램 설정
- 건동 엔지니어링(2018 ~ 2020) 배관 설비
- 트루연팩토리(2017 ~ 2018) 마케팅 업무 담당


## 학력
- 안산대학교 IT비즈니스과 졸업(2016 입학)


## 사용 기술
<img src="images/Untitled.png" width="600"></img>
### Backend
클라이언트에 필요한 기술을 테스트하고 검증할 수 있을 정도의 서버 구현 스킬을 가지고 있습니다.
- java
- spring boot
- docker
- PostgreSQL

### Frontend
주변 도움을 받으면 간단한 프로토타입을 만들 수 있을 정도의 스킬을 가지고 있습니다.
- vue.js
- flutter

그 외 흔히 채용공고에 나오는 이런 '자격요건'들을 경험 해봤습니다.
- 커뮤니케이션 : Slack
- 협업 : GitLab

## 프로젝트 이력

### 리뷰 다있소
<img src="images/Review.png" width="150"></img>

- 소개: **다이소를 이용하는 사람들이 조금 더 편하게 다이소를 이용 할 수 있도록 만든 플랫폼입니다.**
- 기간: 2023.12 ~ 2024.02
- 프로젝트 : 게시판 형태의 프로젝트 Api 구현
- 관리자(웹), 사용자(앱)
- <a href="https://flowery-clementine-57f.notion.site/Project1-Review-Daitso-4e45e801fde9444fa043d5000f69869f?pvs=4">자세한 설명 링크</a>
- <a href="https://www.youtube.com/watch?v=1CxtEgtYWEc&t=35s">유튜브 영상</a>
- <a href="https://gitlab.com/chlwodh97/reviewdaitsoapi">GITLAB</a>
  

### 내 알바야
<img src="images/logo.png" width="150"></img>
- 소개: **혼자서 알바생을 관리하기 힘든 사장님들을 위한 웹사이트 , 어플리케이션 입니다**
- 기간: 2024.3 ~ 2024.05
- 프로젝트 : 알바생 앱에서 위도, 경도에 따른 위치기반 출퇴근 체크 기능 등
- 관리자(웹), 사장님(웹), 사장님(앱), 알바생(앱)
- <a href="https://flowery-clementine-57f.notion.site/Project2-MyWoker-d36eed82e134480da0e10065c95359ba">자세한 설명 링크</a>
- <a href="https://www.youtube.com/watch?v=TT2fAmvc3mc&t=3s">유튜브 영상(사장님)</a>
- <a href="https://www.youtube.com/watch?v=-GLuWnb4N2M&t=6s">유튜브 영상(알바생 + 관리자)</a>
- <a href="https://gitlab.com/chlwodh97/myworkerapi">GITLAB</a>





여기까지 읽어주셔서 감사합니다.

**추가적으로 궁금하신 사항이나 하실 말씀이 있으시면 메일로 연락주세요!**
**chlwodh97@gmail.com**